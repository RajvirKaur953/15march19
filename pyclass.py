class myClass:
    a='Class Attribute'
    b='Another att'

    def intro(self,name,color):
        return 'color of {} is {}'.format(name,color)

ob=myClass()
ob1=myClass()
# print(ob)                       #shows memory location
# print(ob.a)
# print(ob.__class__.a)               #syntatic form

obj1=myClass()
print(obj1.b)

ent1=ob.intro('Parrot','green')
print(ob1.intro('Cow','White'))
print(ent1)