class area:
    ## CLASS ATTRIBUTES
    p=3.14
    # CLASS METHOD
    def circle(self,r):
        return self.p*r*r

    # def square(self,side):
    #     return side*side

    def param(self,r):
        return 2*self.p*r

# #creating object
ob1=area()
# access attribute of a class
print(ob1.p)

# access method of a class 
print(ob1.circle(10))

# input from user
radius=int(input('Enter Circle Radius: '))
print('Area of Circle is {}'.format(ob1.circle(radius)))

# parameter of circle
radius=int(input('Enter Circle Radius: '))
print('Paramete of Circle is {}'.format(ob1.param(radius)))

